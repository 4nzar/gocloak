package internal

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"time"
)

func prepare() (context.Context, context.CancelFunc, string, string) {
	ctx := context.Background()
	timeout := 30 * time.Second
	url, ok := os.LookupEnv("KC_URL")
	if !ok {
		url = "http://localhost:8080"
	}
	realm, ok := os.LookupEnv("KC_PLATFORM_REALM")
	if !ok {
		realm = "realm_default"
	}
	add_context, cancel := context.WithTimeout(ctx, timeout)
	return add_context, cancel, url, realm
}

func ToJSON(T any) ([]byte, error) {
	fmt.Println("TO JSON")
	return json.Marshal(T)
}

func ParseJSON[T any](s []byte) (T, error) {
	fmt.Println("PARSE_JSON")
	var r T
	fmt.Printf("parseJSON: s: %v\n", s)
	if err := json.Unmarshal(s, &r); err != nil {
		fmt.Printf("error: %v\n", err.Error())
		return r, err
	}
	return r, nil
}

func GetFmtURL(admin bool, route string) (context.Context, context.CancelFunc, string) {
	fmt.Println("GET FORMATTED URL")
	ctx, cancel, url, realm := prepare()
	is_old, ok := os.LookupEnv("KC_OLD_VERSION")
	if !ok {
		is_old = "0"
	}

	result := fmt.Sprintf("%s/realms/%s/%s", url, realm, route)
	if admin {
		result = fmt.Sprintf("%s/admin/realms/%s/%s", url, realm, route)
	}

	if is_old == "1" {
		result = fmt.Sprintf("%s/auth/realms/%s/%s", url, realm, route)
		if admin {
			result = fmt.Sprintf("%s/auth/admin/realms/%s/%s", url, realm, route)
		}
	}

	return ctx, cancel, result
}
