package internal

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
)

func Post[T any](ctx context.Context, token, url string, data T) (any, int16, error) {
	fmt.Println("HTTP_POST_REQUEST")
	var m T

	b, err := ToJSON(data)
	if err != nil {
		return m, 500, err
	}
	byte_reader := bytes.NewReader(b)
	request, err := http.NewRequestWithContext(ctx, "POST", url, byte_reader)
	log.Println("1")
	if err != nil {
		return m, 500, err
	}
	log.Println("2")
	request.Header.Add("Content-Type", "application/json")
	log.Println("3")
	if len(strings.TrimSpace(token)) != 0 {
		request.Header.Add("Authorization", token)
	}
	log.Println("4")
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return m, 500, err
	}
	log.Println("5")
	body, err := io.ReadAll(response.Body)
	response.Body.Close()
	if err != nil {
		return m, 500, err
	}
	log.Println("6")
	status_code := response.StatusCode
	log.Printf("Status code : %v\n", status_code)
	log.Printf("err : %v\n", err)
	if status_code >= 400 {
		return string(body), int16(status_code), nil
	}
	log.Println("7")
	json, err := ParseJSON[T](body)
	if err != nil {
		return string(body), int16(status_code), nil
	}
	return json, int16(status_code), nil
}

func Get(ctx context.Context, token, url string) ([]byte, int16, error) {
	request, err := http.NewRequestWithContext(ctx, "GET", url, nil)
	if err != nil {
		return []byte{}, 500, err
	}
	if len(strings.TrimSpace(token)) != 0 {
		request.Header.Add("Authorization", token)
		// request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	}
	fmt.Printf("request = %v\n", request)
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return []byte{}, 500, err
	}
	fmt.Printf("response = %v\n", response)
	body, err := io.ReadAll(response.Body)
	response.Body.Close()
	if err != nil {
		return []byte{}, 500, err
	}

	fmt.Println("HTTP Response Status:", response.StatusCode, http.StatusText(response.StatusCode))
	fmt.Printf("body: %v", string(body))
	status_code := response.StatusCode
	return body, int16(status_code), err
}

func Delete(ctx context.Context, token, url string, data any) (int16, error) {
	b, err := ToJSON(data)
	if err != nil {
		return 500, err
	}
	var request *http.Request
	if data != nil {
		byte_reader := bytes.NewReader(b)
		request, err = http.NewRequestWithContext(ctx, "DELETE", url, byte_reader)
	} else {
		request, err = http.NewRequestWithContext(ctx, "DELETE", url, nil)
	}
	if err != nil {
		return 500, err
	}
	request.Header.Add("Content-Type", "application/json")
	if len(strings.TrimSpace(token)) != 0 {
		request.Header.Add("Authorization", token)
	}
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return 500, err
	}
	response.Body.Close()
	status_code := response.StatusCode
	return int16(status_code), nil
}

func Put[T any](ctx context.Context, token, url string, data any) (T, int16, error) {
	var m T

	b, err := ToJSON(data)
	if err != nil {
		return m, 500, err
	}
	byte_reader := bytes.NewReader(b)
	request, err := http.NewRequestWithContext(ctx, "PUT", url, byte_reader)
	if err != nil {
		return m, 500, err
	} // Important to set
	request.Header.Add("Content-Type", "application/json")
	if len(strings.TrimSpace(token)) != 0 {
		request.Header.Add("Authorization", token)
		// request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	}
	fmt.Printf("request = %v\n", request)
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return m, 500, err
	}
	fmt.Printf("response = %v\n", response)
	body, err := io.ReadAll(response.Body)
	response.Body.Close()
	if err != nil {
		return m, 500, err
	}

	fmt.Println("HTTP Response Status:", response.StatusCode, http.StatusText(response.StatusCode))
	fmt.Printf("body: %v", string(body))
	status_code := response.StatusCode
	json, err := ParseJSON[T](body)
	if err != nil {
		return m, int16(status_code), err
	}
	return json, int16(status_code), err
}
