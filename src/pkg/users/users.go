package users

import (
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/4nzar/gocloak/src/internal"
	"gitlab.com/4nzar/gocloak/src/pkg/model"
)

type APIUserRoutes interface {
	CreateUser(token string, body *model.UserRepresentation)
	ReadUser(token, id string)
	UpdateUser(token, id string, body *model.UserRepresentation)
	DeleteUser(token, id string)
	ListUser(token string, offset, limit int)
	FindUserByEmail(token, email string) ([]model.UserRepresentation, error)
	FindUserByUserName(token, username string) ([]model.UserRepresentation, error)
	FindUserByLastName(token, lastname string) ([]model.UserRepresentation, error)
	FindUserByFirstName(token, firstname string) ([]model.UserRepresentation, error)
}

func CreateUser(token string, body *model.UserRepresentation) (any, int16, error) {
	fmt.Println("CREATE USER")
	fmt.Printf("NEW USER : %v\n", body)
	ctx, cancel, path := internal.GetFmtURL(true, "users")
	defer cancel()
	return internal.Post[*model.UserRepresentation](ctx, token, path, body)
}

func ListUser(token string, offset, limit int) ([]model.UserRepresentation, int16, error) {
	fmt.Println("LIST USERS")
	ctx, cancel, path := internal.GetFmtURL(true, fmt.Sprintf("users?first=%d&max=%d", offset, limit))
	defer cancel()
	response, code, err := internal.Get(ctx, token, path)
	if err != nil {
		return []model.UserRepresentation{}, code, err
	}
	users, err := internal.ParseJSON[[]model.UserRepresentation](response)
	if err != nil {
		return []model.UserRepresentation{}, code, err
	}
	return users, code, nil
}

func FindUserByEmail(token, email string) ([]model.UserRepresentation, int16, error) {
	fmt.Println("FIND USER BY EMAIL")
	ctx, cancel, path := internal.GetFmtURL(true, fmt.Sprintf("users?email=%s", email))
	defer cancel()
	response, code, err := internal.Get(ctx, token, path)
	if err != nil {
		return []model.UserRepresentation{}, code, err
	}
	users, err := internal.ParseJSON[[]model.UserRepresentation](response)
	if err != nil {
		return []model.UserRepresentation{}, code, err
	}
	return users, code, nil
}

func FindUserByUserName(token, username string) ([]model.UserRepresentation, int16, error) {
	fmt.Println("FIND USER BY USERNAME")
	ctx, cancel, path := internal.GetFmtURL(true, fmt.Sprintf("users?username=%s", username))
	defer cancel()
	response, code, err := internal.Get(ctx, token, path)
	if err != nil {
		return []model.UserRepresentation{}, code, err
	}
	users, err := internal.ParseJSON[[]model.UserRepresentation](response)
	if err != nil {
		return []model.UserRepresentation{}, code, err
	}
	return users, code, nil
}

func FindUserByFirstName(token, firstname string) ([]model.UserRepresentation, int16, error) {
	fmt.Println("FIND USER BY FIRSTNAME")
	ctx, cancel, path := internal.GetFmtURL(true, fmt.Sprintf("users?firstname=%s", firstname))
	defer cancel()
	response, code, err := internal.Get(ctx, token, path)
	if err != nil {
		return []model.UserRepresentation{}, code, err
	}
	users, err := internal.ParseJSON[[]model.UserRepresentation](response)
	if err != nil {
		return []model.UserRepresentation{}, code, err
	}
	return users, code, nil
}

func FindUserByLastName(token, lastname string) ([]model.UserRepresentation, int16, error) {
	fmt.Println("FIND USER BY LASTNAME")
	ctx, cancel, path := internal.GetFmtURL(true, fmt.Sprintf("users?lastname=%s", lastname))
	defer cancel()
	response, code, err := internal.Get(ctx, token, path)
	if err != nil {
		return []model.UserRepresentation{}, code, err
	}
	users, err := internal.ParseJSON[[]model.UserRepresentation](response)
	if err != nil {
		return []model.UserRepresentation{}, code, err
	}
	return users, code, nil
}

func DeleteUser(token string, id uuid.UUID) (int16, error) {
	fmt.Println("DELETE USER")
	ctx, cancel, path := internal.GetFmtURL(true, fmt.Sprintf("users/%s", id))
	defer cancel()
	return internal.Delete(ctx, token, path, nil)
}

func GetUser(token string, id uuid.UUID) (model.UserRepresentation, int16, error) {
	fmt.Println("GET USER INFORMATION")
	ctx, cancel, path := internal.GetFmtURL(true, fmt.Sprintf("users/%s", id))
	defer cancel()
	response, code, err := internal.Get(ctx, token, path)
	if err != nil {
		return model.UserRepresentation{}, code, err
	}
	if code != 200 {
		return model.UserRepresentation{}, code, fmt.Errorf("User not found")
	}
	user, err := internal.ParseJSON[model.UserRepresentation](response)
	if err != nil {
		return model.UserRepresentation{}, code, err
	}
	return user, code, nil
}

func GetUserRole(token string, id uuid.UUID) ([]model.UserRole, error) {
	fmt.Println("GET USER ROLE")
	ctx, cancel, path := internal.GetFmtURL(true, fmt.Sprintf("users/%s/groups", id))
	defer cancel()
	response, _, err := internal.Get(ctx, token, path)
	if err != nil {
		return []model.UserRole{}, err
	}
	user_role, err := internal.ParseJSON[[]model.UserRole](response)
	if err != nil {
		return []model.UserRole{}, err
	}
	return user_role, nil
}

func UpdateUser(token string, id uuid.UUID, body *model.UserRepresentation) {
	fmt.Println("UPDATE USER")
	ctx, cancel, path := internal.GetFmtURL(true, fmt.Sprintf("users/%s", id))
	defer cancel()
	internal.Put[any](ctx, token, path, body)
}
