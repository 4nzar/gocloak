package auth

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"gitlab.com/4nzar/gocloak/src/internal"
	"gitlab.com/4nzar/gocloak/src/pkg/model"
)

func GetToken(auth_info model.AuthInfo) (string, error) {
	token := model.AuthToken{}
	now := time.Now()
	var err error
	if now.After(token.ExpiresDate) &&
		now.Before(token.RefreshExpiresDate) {
		token, err = refreshJWT(auth_info, token)
	} else if now.After(token.RefreshExpiresDate) {
		token, err = generateJWT(auth_info, token)
	}
	return token.AccessToken, err
}

func refreshJWT(auth_info model.AuthInfo, token model.AuthToken) (model.AuthToken, error) {
	fmt.Println("REFRESH_TOKEN")
	secret, ok := os.LookupEnv("KC_PLATFORM_CLIENT_SECRET")
	if !ok {
		return model.AuthToken{}, fmt.Errorf("KC_PLATFORM_CLIENT_SECRET variable not found in env")
	}
	form := url.Values{}
	form.Set("grant_type", "refresh_token")
	form.Set("client_id", "platform")
	form.Set("client_secret", secret)
	form.Set("refresh_token", token.RefreshToken)
	return doAuthRequest(auth_info, form, token)
}

func generateJWT(auth_info model.AuthInfo, token model.AuthToken) (model.AuthToken, error) {
	fmt.Println("GENERATE_JWT")
	secret, ok := os.LookupEnv("KC_PLATFORM_CLIENT_SECRET")
	if !ok {
		return model.AuthToken{}, fmt.Errorf("KC_PLATFORM_CLIENT_SECRET variable not found in env")
	}
	form := url.Values{}
	form.Set("grant_type", "password")
	form.Set("client_id", "platform")
	form.Set("client_secret", secret)
	form.Set("username", auth_info.Username)
	form.Set("password", auth_info.Password)
	return doAuthRequest(auth_info, form, token)
}

func doAuthRequest(auth_info model.AuthInfo, form url.Values, token model.AuthToken) (model.AuthToken, error) {
	fmt.Println("DO_AUTH_REQUEST")
	ctx, cancel, path := internal.GetFmtURL(false, "protocol/openid-connect/token")

	if _, ok := os.LookupEnv("KC_PLATFORM_CLIENT_SECRET"); !ok {
		return model.AuthToken{}, fmt.Errorf("KC_PLATFORM_CLIENT_SECRET variable not found in env")
	}
	defer cancel()
	string_reader := strings.NewReader(form.Encode())
	request, err := http.NewRequestWithContext(ctx, "POST", path, string_reader)
	if err != nil {
		return model.AuthToken{}, err
	}
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return model.AuthToken{}, err
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		return model.AuthToken{}, err
	}
	if err := json.Unmarshal(body, &token); err != nil {
		return model.AuthToken{}, fmt.Errorf("failed to unmarshal : %v", err)
	}

	token.ExpiresDate = time.Now().Add(time.Duration(token.ExpiresIn))
	token.RefreshExpiresDate = time.Now().Add(time.Duration(token.RefreshExpiresIn))

	return token, err
}
