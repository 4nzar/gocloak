package model

type APIErrorResponse struct {
	Status     string `json:"status"`
	StatusCode int16  `json:"status_code"`
	Message    any    `json:"message"`
}
