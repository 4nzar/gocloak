package model

type APIResponse[T any] struct {
	Status     string `json:"status"`
	StatusCode int16  `json:"status_code"`
	Data T  `json:"data"`
	Message    any    `json:"message"`
}
