package model

import "github.com/google/uuid"

type UserRepresentation struct {
	Access                     map[string]bool             `json:"access,omitempty"`
	Attributes                 map[string]string           `json:"attributes,omitempty"`
	ClientConsents             []UserConsentRepresentation `json:"clientConsents,omitempty"`
	ClientRoles                map[string]string           `json:"clientRoles,omitempty"`
	CreatedTimestamp           int64                       `json:"createdTimestamp,omitempty"`
	Credentials                []CredentialRepresentation  `json:"credentials,omitempty"`
	DisableableCredentialTypes []string                    `json:"disableableCredentialTypes,omitempty"`
	Email                      string                      `json:"email,omitempty"`
	EmailVerified              bool                        `json:"emailVerified,omitempty"`
	Enabled                    bool                        `json:"enabled,omitempty"`
	FederatedIdentities        []string                    `json:"federatedIdentities,omitempty"`
	FederationLink             string                      `json:"federationLink,omitempty"`
	FirstName                  string                      `json:"firstName,omitempty"`
	Groups                     []string                    `json:"groups,omitempty"`
	ID                         uuid.UUID                      `json:"id,omitempty"`
	LastName                   string                      `json:"lastName,omitempty"`
	NotBefore                  int                         `json:"notBefore,omitempty"`
	Origin                     string                      `json:"origin,omitempty"`
	RealmRoles                 []string                    `json:"realmRoles,omitempty"`
	RequiredActions            []string                    `json:"requiredActions,omitempty"`
	Self                       string                      `json:"self,omitempty"`
	ServiceAccountClientID     string                      `json:"serviceAccountClientId,omitempty"`
	Username                   string                      `json:"username,omitempty"`
}
