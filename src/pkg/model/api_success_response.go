package model

type APISuccessResponse struct {
	Status     string `json:"status"`
	StatusCode int16  `json:"status_code"`
	Data       any    `json:"data"`
}
