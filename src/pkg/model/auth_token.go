package model

import "time"

type AuthToken struct {
	AccessToken        string `json:"access_token"`
	ExpiresIn          int64  `json:"expires_in"`
	RefreshExpiresIn   int64  `json:"refresh_expires_in"`
	RefreshToken       string `json:"refresh_token"`
	TokenType          string `json:"token_type"`
	NotBeforePolicy    int64  `json:"not-before-policy"`
	SessionState       string `json:"session_state"`
	Scope              string `json:"scope"`
	ExpiresDate        time.Time
	RefreshExpiresDate time.Time
}

