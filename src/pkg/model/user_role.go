package model

import "github.com/google/uuid"

type UserRole struct {
	ID   uuid.UUID `json:"id"`
	Name string    `json:"name"`
	Path string    `json:"path"`
}
