package fg

import (
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/4nzar/gocloak/src/internal"
	"gitlab.com/4nzar/gocloak/src/pkg/model"
	"gitlab.com/4nzar/gocloak/src/pkg/users"
)

func respondWithError(ctx *gin.Context, code int, message interface{}) {
	ctx.AbortWithStatusJSON(code, gin.H{"error": message})
}

func getUserInfo(token string) (*model.UserInformation, int16, error) {
	c, cancel, path := internal.GetFmtURL(false, "protocol/openid-connect/userinfo")
	defer cancel()
	response, code, err := internal.Get(c, token, path)
	if err != nil {
		return nil, code, err
	}
	user, err := internal.ParseJSON[model.UserInformation](response)
	if err != nil {
		return nil, 500, err
	}
	return &user, code, nil
}

func hasError(ctx *gin.Context, code int16, err error) bool {
	log.Println("HAS ERROR")
	if err != nil {
		respondWithError(ctx, http.StatusInternalServerError, "An error occured during authorization")
		return true
	}
	if code == 401 {
		respondWithError(ctx, http.StatusUnauthorized, "Unauthorized")
		return true
	}
	if code != 200 {
		respondWithError(ctx, http.StatusInternalServerError, "An error occured during authorization")
		return true
	}
	return false
}

func Identify() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		token := ctx.GetHeader("Authorization")
		if !strings.HasPrefix(token, "Bearer ") {
			respondWithError(ctx, http.StatusUnauthorized, "Bearer token required")
			return
		} else {
			user_info, code, err := getUserInfo(token)
			if has := hasError(ctx, code, err); has {
				return
			}
			var user_roles []model.UserRole
			user_roles, err = users.GetUserRole(token, user_info.ID)
			if err != nil {
				respondWithError(ctx, http.StatusInternalServerError, "An error occured during authorization")
				return
			}
			role := user_roles[0].Name
			if role != "admin" && role != "data_manager" && role != "external_manager" && role != "viewer" && role != "none" {
				respondWithError(ctx, http.StatusUnauthorized, "Unauthorized")
				return
			}
			ctx.Set("issuer_id", user_info.ID)
			ctx.Set("issuer_role", role)
			ctx.Set("token", token)
			ctx.Next()
		}
	}
}

func AdminOnly() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		token := ctx.GetHeader("Authorization")
		if !strings.HasPrefix(token, "Bearer ") {
			respondWithError(ctx, http.StatusUnauthorized, "Bearer token required")
			return
		} else {
			user_info, code, err := getUserInfo(token)
			if has := hasError(ctx, code, err); has {
				return
			}
			var user_roles []model.UserRole
			user_roles, err = users.GetUserRole(token, user_info.ID)
			if err != nil {
				respondWithError(ctx, http.StatusInternalServerError, "An error occured during authorization")
				return
			}
			role := user_roles[0].Name
			if role != "admin" && role != "data_manager" {
				respondWithError(ctx, http.StatusUnauthorized, "Unauthorized")
				return
			}
			ctx.Set("issuer_id", user_info.ID)
			ctx.Set("issuer_role", role)
			ctx.Set("token", token)
			ctx.Next()
		}
	}
}
