package users

import (
	"fmt"
	"os"
	"testing"

	"gitlab.com/4nzar/gocloak/src/pkg/auth"
	"gitlab.com/4nzar/gocloak/src/pkg/model"
	"gitlab.com/4nzar/gocloak/src/pkg/users"
)

func getToken() string {
	auth_info := model.AuthInfo{
		Username: "jdoe@testing.com",
		Password: "1234",
	}
	token, err := auth.GetToken(auth_info)
	if err != nil {
		fmt.Printf("test error %v", err.Error())
		os.Exit(1)
	}
	fmt.Printf("token: %v\n", token)
	return token
}

func getNewUser() model.UserRepresentation {
	return model.UserRepresentation{
		FirstName: "Sadik",
		LastName:  "ESSAIDI",
		Username:  "sessaidi@uby-group.com",
		Email:     "sessaidi@uby-group.com",
		Enabled:   true,
		Groups:    []string{"admin"},
		Credentials: []model.CredentialRepresentation{
			{
				Temporary: true,
				Type:      "password",
				Value:     "1234",
			},
		},
	}
}

func TestCreateUserShouldCreateANewUserAndReturn201(t *testing.T) {
	token := getToken()
	body := getNewUser()
	fmt.Printf("user: %v\n", body)
	response, status_code, err := users.CreateUser(token, &body)
	if err != nil {
		t.Fail()
	}
	if status_code != 201 {
		t.Fail()
	}
	if len(response.(string)) != 0 {
		t.Fail()
	}
}
